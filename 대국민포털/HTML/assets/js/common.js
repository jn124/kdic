// 레이아웃
function fn_layout(){
	var wrap = $("#wrap"),
		header = $("#header"),
		container = $("#container"),
		footer = $("#footer"),
		gnb = header.find("#gnb"),
		depthList = header.find(".depthList");
	
	//전체검색버튼 열고닫기
	header.find(".srchAreaBtn").on('click',function(e){
		if($(this).hasClass("open")){
			$(this).removeClass("open");
			header.find(".srchArea").hide();
			header.find(".inner > div.dim").removeClass("open").hide();
			header.find(".allMenuBtn").css("z-index","2");
		} else {
			$(this).addClass("open");
			header.find(".srchArea").show();
			header.find(".inner > div.dim").addClass("open").show();
			header.find(".allMenuBtn").css("z-index","0");
		}
	});

	//header 전체메뉴 열고닫기
	header.find(".allMenuBtn").on('click',function(e){
		if($(this).hasClass("open")){
			$(this).removeClass("open");
			$(this).siblings(".util").removeClass("open");
			$(".allMenu").css("display","none");
			$(".allMenu *").find(".on").removeClass("on");
			$(".allMenu *").find("dd").slideUp();
			$("body, html").css("overflow","auto");
			if($(window).width() > 767){
				$(this).prev(".srchAreaBtn").css("z-index","2");
			}
			if($(window).width() < 767){
				$(".logo").css("display","block");
				$(this).prev(".srchAreaBtn").css("display","inline-block");
			}
		} else { 
			$(this).addClass("open");
			$(this).siblings(".util").addClass("open");
			$(".allMenu").css("display","block");
			$("body, html").css("overflow","hidden");
			if($(window).width() > 767){
				$(this).prev(".srchAreaBtn").css("z-index","1");
			}
			if($(window).width() < 767){
				$(".logo").css("display","none");
				$(this).prev(".srchAreaBtn").css("display","none");
			}
		}
	});

	//메뉴 tab 포커스이동 
	header.find(".depthList > li > a").focus(function(){
		$(this).next(".subArea").css("display","block");
		$(this).parent("li").siblings("li").find(".subArea").css("display","none");
		$(".allMenuBtn.open").focus();
	});
	$("#gnb").focusout(function(){
		header.find(".subArea").css("display","none");
	});
	$(".allMenuList").find("h2").focus(function(){
		$(this).next(".allMenuInner").css("display","block");
		$(this).parent("div").siblings("div").find(".allMenuInner").css("display","none");
	});
	$(".allMenuInner > ul > li").find("dt").focus(function(){
		$(this).next("dd").css("display","block");
		$(this).parent("li").siblings("li").find("dd").css("display","none");
	});

	// 사용자정보 영역
	$(window).resize(function(){
		if($(window).width() < 1161){
			if(header.hasClass("logIn")){
				$(".allMenu").css("padding-top","88px");
			} else {
				$(".allMenu").css("padding-top","0");
			}
		}
	}).resize();

	//전체메뉴 아코디언
	$(".allMenuList").find(".inner > div > h2").on('click',function(){
		var siblings = $(this).parents("div").siblings("div");

		if($(this).hasClass("on")){
			$(this).removeClass("on").next(".allMenuInner").slideUp(200);
		} else {
			$(this).addClass("on").next(".allMenuInner").slideDown(200);
			siblings.find(">h2").removeClass("on");
			siblings.find(".allMenuInner").slideUp(200);
		}
	});

	//전체메뉴 아코디언 inner
	$(".allMenuList").find("dt").on('click',function(){	
		if($(this).hasClass("on")){
			$(this).removeClass("on").next("dd").slideUp(200);
		} else {
			$(this).addClass("on").next("dd").slideDown(200);
			const offset = $(".mCSB_container").offset().top - $(this).offset().top;
			$(".allMenuList").find("#mCSB_1_container").animate({scrollTop : offset}, 400);
		}
	});
	
	//gnb 열고닫기
	gnb.find(">ul>li").mouseenter(function(e){
		$(this).find(".subArea").css("display","block");
		$(".subArea .inner .scrollArea").css("display","block");
	});
	gnb.find(">ul>li").mouseleave(function(e){
		gnb.find(".subArea").css("display","none");
	});

	//챗봇
	$(".chatbot").mouseenter(function(e){
		$(this).addClass("on");
	});
	$(".chatbot").mouseleave(function(e){
		$(this).removeClass("on");
	});

	//스크롤영역
	if ($(".scrollArea").length) {
		$(".scrollArea").mCustomScrollbar();
	}

	$(window).resize(function(){
		var headerHeight = header.find(".inner").innerHeight(),
			allmenuHeight = $(window).height() - headerHeight;
		//allmenu top
		$(".allMenu").find(".inner").css("top",headerHeight);
		//allmenu height
		$(".allMenu").find(".inner").css("height",allmenuHeight);

		if($(window).width() > 1160){
			/* 웹 */
			//container 상단여백
			container.css("padding-top", header.height());
		}else{
			/* 모바일 */
			//container 상단여백
			container.css("padding-top", header.height());

			// 메뉴
			if(!$(".ddInList > li > a").hasClass(".noDepth")){
				$(".ddInList > li > a").on('click',function(){	
					var innerList = $(this).parent("li").siblings("li");
	
					if($(this).hasClass("open")){
						$(this).parent("li").removeClass("open").find("> ul").slideUp(200);
					} else {
						$(this).parent("li").addClass("open").find("> ul").slideDown(200);
						$(this).parent("li").siblings("li").removeClass("open")
						innerList.find("> ul").slideUp(200);
					}
				});
			}
			var depth04width = $(".depth04").width();
			$(".depth03").on('click',function(){	
				$(this).next(".depth04").addClass("open").css("right", 0);
				$('.dim').addClass('on');
				$('.switchZidx').css('isolation', 'isolate');
			});
			$(".depth04").find(">a").on('click',function(){
				$(this).parent(".depth04").removeClass("open").css("right", "-260px");
				$('.dim').removeClass('on');
				$('.switchZidx').css('isolation', 'auto');
			});
			
		}
	}).resize();

	$(window).resize(function(){
		// location 모바일일때 스크롤 
		if ($(".location").length) {
			var li_one = $(".location").find("li:first-child").outerWidth(),
				li_two = $(".location").find("li:nth-child(2)").outerWidth(),
				li_three = $(".location").find("li:nth-child(3)").outerWidth(),
				li_four = $(".location").find("li:nth-child(4)").outerWidth(),
				liWidth = li_one + li_two + li_three + li_four + 50;

			$(".location").find("ol").css("width", liWidth);
		}

		// window 높이 800일하 일때 gnb 길이조절
		var gnbHeight = $(window).height() - $("#header").outerHeight() - 50;
		if($(window).height() < 800){
			$("#header").find("#gnb").find(".mCustomScrollbar").css("height",gnbHeight);
		}
	}).resize();  
}

// 레이어 팝업
function fn_layer(e,t,s) {
	var pdt = $('#'+e).find('> .inner').css('padding-top').replace(/[^-\d\.]/g, ''),
		pdb = $('#'+e).find('> .inner').css('padding-bottom').replace(/[^-\d\.]/g, '');
	$('#'+e).fadeIn(200).addClass('on');
	$('body, html').css({'overflow':'hidden'});
	 $('#'+e).attr("tabindex",0).focus();
	$(window).resize(function(){
		$('#'+e).find('> .inner').css({'width':s+'px'});
		if($(window).width() > 767){
			$('#'+e).find('.cont').css({'max-height':$('#'+e).height()*0.9 - (Number(pdt) + Number(pdb))});
		}else{
			$('#'+e).find('.cont').css({'max-height':$('#'+e).height() - (Number(pdt) + Number(pdb))});
		}

		if($(".recom").length){
			$(".recom ul").css({"max-height":$('#'+e).find('.cont').height() - $(".recom .btnArea").outerHeight()});
		}
	}).resize();

	if($(t).closest(".layerPop").hasClass("popClose")){
		$(t).closest(".layerPop").fadeOut(200).removeClass("on");
		$("body, html").css({"overflow":"auto"});
	}
	$(t).addClass(e);
}

// 레이어 팝업 닫기
function fn_layer_close(t){
	var backFocus = $(t).closest(".layerPop").attr("id");
	$(t).closest(".inner").parent().fadeOut(200).removeClass("on");
	$("body, html").css({"overflow":"auto"});
	$("." + backFocus).focus();
}
$(function() {
	/*>>>>>>>>>> 공통 <<<<<<<<<<*/
	fn_layout(); // 레이아웃

	// 스킵네비
	$("a[href^='#']").click(function(evt){
	  var anchortarget = $(this).attr("href");
	  $(anchortarget).attr("tabindex", -1).focus();
	  $(anchortarget).removeAttr("tabindex");
	 });
	if (window.location.hash) {
		$(window.location.hash).attr("tabindex", -1).focus();
	};
	var skipNav = $("#skipNav a");
	skipNav.focus(function(){
		skipNav.removeClass("on");
		$(this).addClass("on");
	});
	skipNav.blur(function(){
		skipNav.removeClass("on");
	});

	$('.btnScrolltop').on('click', function(){
		$('html, body').animate({
		  scrollTop: 0
		},500);
	});

	// 탭
	var tabArea = $(".tabFunc");
	tabArea.each(function(){
		var tabBox = $(this).find("> div"),
			btnTab = $(this).find("> ul li a"),
			curr = $(this).find("> ul li.curr a"),
			currId = curr.attr("href"),
			currText = curr.text();
		curr.attr("title","현재 페이지");
		$("#"+currId).show().prepend("<h3 class='hide'>"+currText+"</h3>");
		btnTab.click(function(e){
			e.preventDefault();
			var id = $(this).attr("href"),
				tit = $(this).text();
			btnTab.removeAttr("title").parent().removeClass("curr");
			$(this).attr("title","현재 페이지").parent().addClass("curr");
			tabBox.hide().find("h3.hide").remove();
			$("#"+id).show().prepend("<h3 class='hide'>"+tit+"</h3>");
		});
	});

	//체크박스 전체선택
	$(".checkbox").on('click','#checkAll',function(){
		if($('#checkAll').is(':checked')){
            $('.checkList').prop('checked',true).closest("tr").addClass("checked"); 
        }else{
		    $('.checkList').prop('checked',false).closest("tr").removeClass("checked"); 
		}
	});
	$(".checkbox").on('click','.checkList',function(){
        if($('input[class=checkList]:checked').length == $('.checkList').length){
            $('#checkAll').prop('checked',true);
        }else{
            $('#checkAll').prop('checked',false);
        }
    });

	// 체크박스 클릭시 tr배경색 변경
	$(".listTable").find("td:first-child > .checkbox > input[type='checkbox']:not(#checkAll)").change(function (e) {
		if ($(this).is(":checked")) { 
			$(this).closest("tr").addClass("checked"); 
		} else {
			$(this).closest("tr").removeClass("checked");
		}
	});

	// 데이트피커
	if($(".date").length){
		$(".date input").datepicker({
			dateFormat: 'yy-mm-dd',
			showOtherMonths: true,
			showMonthAfterYear:true,
			changeYear: true,
			changeMonth: true,          
			showOn: "both",
			buttonImage: "../assets/images/btn-date.png",
			buttonImageOnly: true,
			buttonText: "선택",
			monthNamesShort: ['1','2','3','4','5','6','7','8','9','10','11','12'],
			monthNames: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
			dayNamesMin: ['일','월','화','수','목','금','토'],
			dayNames: ['일요일','월요일','화요일','수요일','목요일','금요일','토요일']
		});
	}

	// ROW 테이블 높이값
	$(window).resize(function(){
		var rowTable = $(".colspan");
		rowTable.find("th").each(function(){
			var td = $(this).next("td");
			$(this).outerHeight(td.outerHeight());
		});
	}).resize();

	// 상단 타이틀 sns리스트 (모바일)
	$(".snsBtn").on('click', function(){
		if($(this).hasClass("on")){
			$(this).removeClass("on").next("ul").slideUp(200);
		}else{
			$(this).addClass("on").next("ul").slideDown(200);
		}
	});
	
	/*>>>>>>>>>> 페이지 <<<<<<<<<<*/
	// 착오송금반환지원_소개와방법안내_상황선택
	$(window).resize(function(){
		var situChoiceImg = $(".situChoice > div + div").find("ul > li").find("img").width();
		var situChoiceHeight = $(".situChoice > div + div").find("ul > li").height();
		$(".situChoice > div:first-child").find("ul > li").find("img").css("height",situChoiceImg);
		$(".situChoice > div:first-child").find("ul > li").css("height",situChoiceHeight);
	}).resize();	
});
