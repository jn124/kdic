$(function() {
	$("#container").addClass("main");
	/******* 메인 공통 *******/
	$(window).resize(function(){
		visualImg = $(".visualSwiper").find(".swiper-slide > img");
		visualImg.each(function(){
			if($(window).width() > 767){
					$(this).attr("src", $(this).attr("src").replace("-m","-pc"));
			} else{
					$(this).attr("src", $(this).attr("src").replace("-pc", "-m"));
			}
		});


		var visualHeight = $(".visualSwiper").find(".swiper-slide").innerHeight(),
			tabUlHeight = $(".section.v1").find(".tabArea > ul").outerHeight(),
		    sliderHeight = visualHeight - tabUlHeight - 16,
			slideHeight = sliderHeight / 3;

		$(".section.v1").find(".slider").find(".swiper-slide").css("height",slideHeight);

		if($(window).width() > 767){
			// 공지사항
			var noticeSwiper = new Swiper(".noticeSwiper", {
				slidesPerView: 4,
				observer: true,
				observeParents: true,
				navigation: {
					nextEl: ".tabBtnNext.notice",
					prevEl: ".tabBtnPrev.notice",
				},
				breakpoints: {
					767: {
						slidesPerView: "auto",
					},
					1160: {
						slidesPerView: 3,
					},
				},
			});
			// 보도자료
			var dataSwiper = new Swiper(".dataSwiper", {
				slidesPerView: 4,
				observer: true,
				observeParents: true,
				navigation: {
					nextEl: ".tabBtnNext.data",
					prevEl: ".tabBtnPrev.data",
				},
				breakpoints: {
					767: {
						slidesPerView: "auto",
					},
					1160: {
						slidesPerView: 3,
					},
				},
			});
			// KDIC 최신보고서
			var kdicSwiper = new Swiper(".kdicSwiper", {
				observer: true,
				observeParents: true,
				slidesPerView: 4,
				navigation: {
					nextEl: ".tabBtnNext.kdic",
					prevEl: ".tabBtnPrev.kdic",
				},
				breakpoints: {
					767: {
						slidesPerView: "auto",
					},
					1160: {
						slidesPerView: 3,
					},
				},
			});
		}else{
			
		}
	}).resize();

	/*******  *******/
	// 비주얼
	var visualSwiper = new Swiper(".visualSwiper", {
		slidesPerView: 1,
		observer: true,
		observeParents: true,
		navigation: {
			nextEl: ".btnNext.visual",
			prevEl: ".btnPrev.visual",
		},
		pagination: {
			el: ".swiper-pagination",
		},
	});

	//자주찾는메뉴
	var menuSwiper = new Swiper(".menuSwiper", {
		slidesPerView: 3,
		slidesPerColumn: 3,
		slidesPerGroup :3,
		spaceBetween: 1,
		pagination: {
			el: '.swiper-pagination',
			type: "fraction",
		},
		navigation: {
			nextEl: ".btnNext.favorite",
			prevEl: ".btnPrev.favorite",
		},
		breakpoints: {
			767: {
				slidesPerView: 2,
				slidesPerColumn: 2,
				slidesPerGroup :2,
			},
		},
		observer: true,
		observeParents: true,
    });

	//나의메뉴
	var myMenuSwiper = new Swiper(".myMenuSwiper", {
		slidesPerView: 3,
		slidesPerColumn: 3,
		slidesPerGroup :3,
		spaceBetween: 1,
		pagination: {
			el: '.swiper-pagination',
			type: "fraction",
		},
		navigation: {
			nextEl: ".btnNext.myMenu",
			prevEl: ".btnPrev.myMenu",
		},
		breakpoints: {
			767: {
				slidesPerView: 2,
				slidesPerColumn: 2,
				slidesPerGroup :2,
			},
		},
		observer: true,
		observeParents: true,
    });	

	// 배너
	var bannerSwiper = new Swiper(".bannerSwiper", {
		pagination: {
			el: ".swiper-pagination",
		},
		autoplay: {
			delay: 4000,
			disableOnInteraction: false,
		},
		loop: true,
	});

	// SNS
	var snsSwiper = new Swiper(".snsSwiper", {
		slidesPerView: "auto",
		spaceBetween: 55,
		breakpoints: {
			767: {
				spaceBetween: 15,
			},
			1160: {
				spaceBetween: 35,
			},
		},
	});

	/*******  *******/


	/*******  *******/
	// 


	//

});