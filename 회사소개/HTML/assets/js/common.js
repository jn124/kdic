// 레이아웃
function fn_layout(){
	var wrap = $("#wrap"),
		header = $("#header"),
		container = $("#container"),
		footer = $("#footer"),
		gnb = header.find("#gnb"),
		depthList = header.find(".depthList");
	
	//header 전체메뉴 열고닫기
	header.find(".allMenuBtn").on('click',function(e){
		if($(this).hasClass("open")){
			$(this).removeClass("open");
			$(this).prev(".util").removeClass("open");
			$(".allMenu").css("display","none");
			$(".allMenu *").find(".on").removeClass("on");
			$(".allMenu *").find("dd").slideUp();
			$("body, html").css("overflow","auto");
			if($(window).width() < 767){
				$(".logo").css("display","block");
			}
		} else { 
			$(this).addClass("open");
			$(this).prev(".util").addClass("open");
			$(".allMenu").css("display","block");
			$("body, html").css("overflow","hidden");
			if($(window).width() < 767){
				$(".logo").css("display","none");
			}
		}
	});

	//전체메뉴 아코디언
	$(".allMenuList").find(".inner > div > h2").on('click',function(){
		var siblings = $(this).parents("div").siblings("div");

		if($(this).hasClass("on")){
			$(this).removeClass("on").next(".allMenuInner").slideUp(200);
		} else {
			$(this).addClass("on").next(".allMenuInner").slideDown(200);
			siblings.find(">h2").removeClass("on");
			siblings.find(".allMenuInner").slideUp(200);
		}
	});
	$(".allMenuList").find("dt").on('click',function(){	
		if($(this).hasClass("on")){
			$(this).removeClass("on").next("dd").slideUp(200);
		} else {
			$(this).addClass("on").next("dd").slideDown(200);
			// var offset = $(".allMenuList").find(".inner").offset();
			// $("html").animate({scrollTop : offset.top}, 400);
		}
	});
	

	//gnb 열고닫기
	gnb.find(">ul>li").mouseenter(function(e){
		$(this).find(".subArea").css("display","block");
		$(".subArea .inner .scrollArea").css("display","block");
	});
	gnb.find(">ul>li").mouseleave(function(e){
		gnb.find(".subArea").css("display","none");
	});

	// 언어설정
	$(".lang").find(">span").on('click',function(){	
		if($(this).hasClass("on")){
			$(this).removeClass("on");
			$(this).next("ul").slideUp(200);
		} else {
			$(this).addClass("on");
			$(this).next("ul").slideDown(200);
		}
	});

	//챗봇
	$(".chatbot").mouseenter(function(e){
		$(this).addClass("on");
	});
	$(".chatbot").mouseleave(function(e){
		$(this).removeClass("on");
	});

	//스크롤영역
	if ($(".scrollArea").length) {
		$(".scrollArea").mCustomScrollbar();
	}

	// footer 관련사이트
	var familySite = footer.find(".familySite");

	familySite.find("> a").click(function(e){
		e.preventDefault();
		if($(this).parent().hasClass("on")){
			$(this).parent().removeClass("on");
			$(this).siblings("ul").slideUp(200);
		}else{
			$(this).parent().addClass("on");
			$(this).siblings("ul").slideDown(200);
		}
	});

	familySite.find("ul li:last-child a").focusout(function(){
		familySite.removeClass("on").find("ul").slideUp(200);
	});

	$(window).resize(function(){
		var headerHeight = header.find(".inner").innerHeight(),
			allmenuHeight = $(window).height() - headerHeight;
		//allmenu top
		$(".allMenu").find(".inner").css("top",headerHeight);
		//allmenu height
		$(".allMenu").find(".inner").css("max-height",allmenuHeight);

		if($(window).width() > 1160){
			/* 웹 */
			//container 상단여백
			container.css("padding-top", header.height());
		}else{
			/* 모바일 */
			//container 상단여백
			container.css("padding-top", header.height());

			// 메뉴
			if(!$(".ddInList > li > a").hasClass(".noDepth")){
				$(".ddInList > li > a").on('click',function(){	
					var innerList = $(this).parent("li").siblings("li");
	
					if($(this).hasClass("open")){
						$(this).parent("li").removeClass("open").find("> ul").slideUp(200);
					} else {
						$(this).parent("li").addClass("open").find("> ul").slideDown(200);
						$(this).parent("li").siblings("li").removeClass("open")
						innerList.find("> ul").slideUp(200);
					}
				});
			}
			var depth04width = $(".depth04").width();
			$(".depth03").on('click',function(){	
				$(this).next(".depth04").addClass("open").css("right", 0);
				$('.dim').addClass('on');
				$('.switchZidx').css('isolation', 'isolate');
			});
			$(".depth04").find(">a").on('click',function(){
				$(this).parent(".depth04").removeClass("open").css("right", "-260px");
				$('.dim').removeClass('on');
				$('.switchZidx').css('isolation', 'auto');
			});
			
		}
	}).resize();
}

// 레이어 팝업
function fn_layer(e,t,s) {
	var pdt = $('#'+e).find('> .inner').css('padding-top').replace(/[^-\d\.]/g, ''),
		pdb = $('#'+e).find('> .inner').css('padding-bottom').replace(/[^-\d\.]/g, '');
	$('#'+e).fadeIn(200).addClass('on');
	$('body, html').css({'overflow':'hidden'});
	 $('#'+e).find('> .inner .cont').attr("tabindex",0).focus();
	$(window).resize(function(){
		$('#'+e).find('> .inner').css({'width':s+'px'});
		if($(window).width() > 767){
			$('#'+e).find('.cont').css({'max-height':$('#'+e).height()*0.9 - (Number(pdt) + Number(pdb))});
		}else{
			$('#'+e).find('.cont').css({'max-height':$('#'+e).height() - (Number(pdt) + Number(pdb))});
		}
	}).resize();
	$(t).addClass(e);
}

// 레이어 팝업 닫기
function fn_layer_close(t){
	var backFocus = $(t).closest(".layerPop").attr("id");
	$(t).closest(".inner").parent().fadeOut(200).removeClass("on");
	$("body, html").css({"overflow":"auto"});
	$("." + backFocus).focus();
}
$(function() {
	/*>>>>>>>>>> 공통 <<<<<<<<<<*/
	fn_layout(); // 레이아웃

	// 스킵네비
	$("a[href^='#']").click(function(evt){
	  var anchortarget = $(this).attr("href");
	  $(anchortarget).attr("tabindex", -1).focus();
	  $(anchortarget).removeAttr("tabindex");
	 });
	if (window.location.hash) {
		$(window.location.hash).attr("tabindex", -1).focus();
	};
	var skipNav = $("#skipNav a");
	skipNav.focus(function(){
		skipNav.removeClass("on");
		$(this).addClass("on");
	});
	skipNav.blur(function(){
		skipNav.removeClass("on");
	});

	$('.btnScrolltop').on('click', function(){
		$('html, body').animate({
		  scrollTop: 0
		},500);
	});

	// $(window).on('mousewheel',function(e){ 
	// 	var scrT = $(window).scrollTop();
	// 	if(scrT == $(document).height() - $(window).height()){
	// 		$(".floatBottom").css("bottom","250px");
	// 	} else {
	// 		$(".floatBottom").css("bottom","20px");
	// 	}
	// });

	// 탭
	var tabArea = $(".tabFunc");
	tabArea.each(function(){
		var tabBox = $(this).find("> div"),
			btnTab = $(this).find("> ul li a"),
			curr = $(this).find("> ul li.curr a"),
			currId = curr.attr("href"),
			currText = curr.text();
		curr.attr("title","현재 페이지");
		$("#"+currId).show().prepend("<h3 class='hide'>"+currText+"</h3>");
		btnTab.click(function(e){
			e.preventDefault();
			var id = $(this).attr("href"),
				tit = $(this).text();
			btnTab.removeAttr("title").parent().removeClass("curr");
			$(this).attr("title","현재 페이지").parent().addClass("curr");
			tabBox.hide().find("h3.hide").remove();
			$("#"+id).show().prepend("<h3 class='hide'>"+tit+"</h3>");
		});
	});

	/*>>>>>>>>>> 페이지 <<<<<<<<<<*/
});
